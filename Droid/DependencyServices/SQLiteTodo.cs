﻿using System;
using SQLite.Net.Interop;
using Xamarin.Forms;
using sampleTodo.Droid.DependencyServices;
using sampleTodo.Services.Database;
using System.IO;
using sampleTodo.Helpers;

[assembly: Dependency(typeof(SQLiteContent))]
namespace sampleTodo.Droid.DependencyServices
{
	public class SQLiteContent : ISQLiteContent
	{
		#region ISQLite implementation

        public SQLite.Net.SQLiteConnection GetConnection()
		{
			var fileName = Constants.DataBaseName;
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var path = Path.Combine(documentsPath, fileName);

            var connection = new SQLite.Net.SQLiteConnection (new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid(), path);

			return connection;
		}

		public ISQLitePlatform GetPlatform()
		{
			var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();// .SQLiteApiAndroid();

			return platform;
		}

		public string GetPath()
		{
			var fileName = Constants.DataBaseName;
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var path = Path.Combine(documentsPath, fileName);

			return path;
		}

		#endregion
	}
}
