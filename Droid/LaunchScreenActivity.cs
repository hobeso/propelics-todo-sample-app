﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;

namespace sampleTodo.Droid
{
	[Activity(MainLauncher = true, NoHistory = true, Theme = "@style/SplashTheme")] //Doesn't place it in back stack
	public class LaunchScreenActivity : AppCompatActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			StartActivity(new Intent(Application.Context, typeof(MainActivity)));
		}

		protected override void OnResume()
		{
			base.OnResume();
		}
	}
}
