﻿using SQLite.Net.Interop;
using Xamarin.Forms;
using sampleTodo.iOS.DependencyServices;
using sampleTodo.Services.Database;
using System;
using System.IO;

[assembly: Dependency(typeof(SQLiteTodo))]
namespace sampleTodo.iOS.DependencyServices
{
    public class SQLiteTodo: ISQLiteContent
    {
		#region ISQLite implementation


		public string GetPath()
		{
			var fileName = Helpers.Constants.DataBaseName;
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var libraryPath = Path.Combine(documentsPath, "..", "Library");
			var path = Path.Combine(libraryPath, fileName);
			Console.WriteLine(string.Format("{0}", path));
			return path;
		}

		public ISQLitePlatform GetPlatform()
		{
			var platform = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS();

			return platform;
		}

        public SQLite.Net.SQLiteConnection GetConnection()
		{
			var fileName = Helpers.Constants.DataBaseName;
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var libraryPath = Path.Combine(documentsPath, "..", "Library");
			var path = Path.Combine(libraryPath, fileName);

            var connection = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS(), path);

			return connection;
		}

		#endregion
	}
}
