﻿using System;
namespace sampleTodo.Helpers
{
    public class Constants
    {
		#region General Data
        /// <summary>
        /// The name of the app.
        /// </summary>
		public static readonly string AppName = "Sample Todo";
		
        /// <summary>
        /// The name of the data base.
        /// </summary>
		public static readonly string DataBaseName = "sampleTodo.db12";
		
        /// <summary>
        /// The image default.
        /// </summary>
        public static readonly string ImageDefault = "noteImage.png"; 
		#endregion
	}
}
