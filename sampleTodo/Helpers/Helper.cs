﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using System.Windows.Input;
using System.ComponentModel;
using System.IO;
using System.Diagnostics;

namespace sampleTodo.Helpers
{
    public class Helper
    {
        /// <summary>
        /// Reads the stream.
        /// </summary>
        /// <returns>The stream.</returns>
        /// <param name="input">Input.</param>
        public static byte[] ReadStream(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Reads the bytes.
        /// </summary>
        /// <returns>The bytes.</returns>
        /// <param name="buffer">Buffer.</param>
        public static string ReadBytes(byte[] buffer)
        {
            try
            {
                if (buffer != null)
                {
                    var imageBase64 = Convert.ToBase64String(buffer);
                    return imageBase64;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());

                return string.Empty;
            }
        }

        /// <summary>
        /// Reads the base64.
        /// </summary>
        /// <returns>The base64.</returns>
        /// <param name="base64">Base64.</param>
        public static ImageSource ReadBase64(string base64)
        {
            try
            {
                if (string.IsNullOrEmpty(base64))
                {
                    return new FileImageSource { File = Constants.ImageDefault };
                }

                if (base64.Length <= 10)
				{
					return new FileImageSource { File = Constants.ImageDefault };
				}

                var imageBytes = Convert.FromBase64String(base64);

                if (imageBytes == null)
                {
                    return new FileImageSource { File = Constants.ImageDefault };
                }

                MemoryStream st = new MemoryStream(imageBytes);

                if (st == null)
                {
                    return new FileImageSource { File = Constants.ImageDefault };
                }

                return ImageSource.FromStream(() => st);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());

                return new FileImageSource { File = Constants.ImageDefault };
            }

        }

        /// <summary>
        /// Log the specified ex.
        /// </summary>
        /// <returns>The log.</returns>
        /// <param name="ex">Ex.</param>
        public static void Log(Exception ex)
        {
            Debug.WriteLine(ex.ToString());
        }
    }
}
