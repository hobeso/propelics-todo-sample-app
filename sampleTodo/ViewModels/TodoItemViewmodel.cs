﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Share;
using sampleTodo.Helpers;
using sampleTodo.Models;
using sampleTodo.Services.Database;
using Xamarin.Forms;

namespace sampleTodo.ViewModels
{
    public class TodoItemViewmodel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Action NavigateBack;
		public Action<TodoItem> NavigateToFullImage;
        public Command SaveTodoCommand { get; private set; }
        public Command AddPictureCommand { get; private set; }
        public Command DeleteTodoCommand { get; private set; }
        public Command ShareTodoCommand { get; private set; }
		public Command SeePictureCommand { get; private set; }

        private TodoItem currentTodoItem { get; set; }

        #region Properties
        private bool _IsValid = false;
        public bool IsValid
        {
            get { return _IsValid; }
            set
            {
                _IsValid = value;

                SaveTodoCommand.ChangeCanExecute();

                OnPropertyChanged("IsValid");
            }
        }


        private bool _IsNew = true;
        public bool IsNew
        {
            get { return _IsNew; }
            set
            {
                _IsNew = value;

                SaveTodoCommand.ChangeCanExecute();
                DeleteTodoCommand.ChangeCanExecute();
                ShareTodoCommand.ChangeCanExecute();

                OnPropertyChanged("IsNew");

                OnPropertyChanged("IsEditing");
            }
        }

        public bool IsEditing
        {
            get { return !IsNew; }
        }

        private string _Content = string.Empty;
        public string Content
        {
            get { return _Content; }
            set
            {
                _Content = value;

                if (!string.IsNullOrEmpty(_Content))
                    IsValid = true;
                else
                    IsValid = false;

                OnPropertyChanged("Content");
            }
        }


        private byte[] ImageRaw { get; set; }

        private ImageSource _SourceImg;
        public ImageSource SourceImg
        {
            get { return _SourceImg; }
            set
            {
                _SourceImg = value;

                OnPropertyChanged("SourceImg");
            }
        }

        private bool _Status = false;
        public bool Status
        {
            get { return _Status; }
            set
            {
                _Status = value;

                OnPropertyChanged("Status");

                StatusTitle = ((_Status == true) ? "Completed" : "Pending");
            }
        }

        private string _StatusTitle = "Pending";
        public string StatusTitle
        {
            get { return _StatusTitle; }
            set
            {
                _StatusTitle = value;

                OnPropertyChanged("StatusTitle");
            }
        }


        private string _Name = "Add Todo";
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;

                OnPropertyChanged("Name");
            }
        }

        private string _LastUpdate = string.Empty;
        public string LastUpdate
        {
            get { return _LastUpdate; }
            set
            {
                _LastUpdate = value;

                OnPropertyChanged("LastUpdate");
            }
        }
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="T:sampleTodo.ViewModels.TodoItemViewmodel"/> class.
        /// </summary>
        /// <param name="SelectedItem">Selected item.</param>
        public TodoItemViewmodel(TodoItem SelectedItem = null)
        {
            SaveTodoCommand = new Command(async () => await AddNote(), () => IsValid);

            AddPictureCommand = new Command(async () => await TakePicture());

            DeleteTodoCommand = new Command(DeleteTodo, () => !IsNew);

            ShareTodoCommand = new Command(ShareTodo, () => !IsNew);

            SeePictureCommand = new Command(SeeImage, () => !IsNew);

            SourceImg = new FileImageSource { File = Constants.ImageDefault };

            if (SelectedItem != null)
            {
                currentTodoItem = SelectedItem;
                IsNew = false;

                Content = currentTodoItem.Content;

                Status = ((currentTodoItem.Status == TodoStatus.Completed) ? true : false);

                SourceImg = Helper.ReadBase64(currentTodoItem.Picture);

                LastUpdate = string.Format("{0} at {1}", currentTodoItem.LastModified.ToString("dddd dd MMMM"), currentTodoItem.LastModified.ToString("hh:mm"));

                Name = "Edit Note";
            }

        }

        /// <summary>
        /// Ons the property changed.
        /// </summary>
        /// <param name="name">Name.</param>
        void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Takes the picture.
        /// </summary>
        /// <returns>The picture.</returns>
        private async Task<bool> TakePicture()
        {
            try
            {
                List<string> OptionsAvailables = new List<string>();

                if (!CrossMedia.Current.IsCameraAvailable && !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await Application.Current.MainPage.DisplayAlert("No Camera/Pictures", ":( No Camera/Pictures avaialble.", "OK");
                    return false;
                }

                if (CrossMedia.Current.IsCameraAvailable)
                {
                    OptionsAvailables.Add("Take a Picture");
                }


                if (CrossMedia.Current.IsTakePhotoSupported)
                {
                    OptionsAvailables.Add("Photo Roll");
                }


                var action = await Application.Current.MainPage.DisplayActionSheet("Set a Picture", "Cancel", null, OptionsAvailables.ToArray());


                MediaFile file = null;


                switch (action)
                {
                    case "Take a Picture":
                        {
                            file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions()
                            {
                                Name = $"{DateTime.UtcNow}.jpg"
                            });
                            break;
                        }
                    case "Photo Roll":
                        {
                            file = await CrossMedia.Current.PickPhotoAsync();
                            break;
                        }

                    default:
                        return false;
                }

                if (file != null)
                {
                    ImageRaw = Helper.ReadStream(file.GetStream());

                    SourceImg = ImageSource.FromStream(() =>
                        {
                            var stream = file.GetStream();
                            file.Dispose();
                            return stream;
                        });
                }

            }
            catch (Exception ex)
            {
                Helper.Log(ex);
            }

            return true;
        }

        /// <summary>
        /// Adds the note.
        /// </summary>
        private async Task<int> AddNote()
        {
            if (!IsNew)
            {
                UpdateNote();
                return 1;
            }


			await Task.Run((delegate
			{
				var todoIetm = new TodoItem();

				todoIetm.Content = Content;

				todoIetm.Status = TodoStatus.Pending;

				if (ImageRaw != null)
					todoIetm.Picture = Helper.ReadBytes(ImageRaw);

				TodoItemDatabase.AddTodo(todoIetm);

				currentTodoItem = todoIetm;

                Device.BeginInvokeOnMainThread(() =>
                {
					LastUpdate = string.Format("{0} at {1}", todoIetm.LastModified.ToString("dddd dd MMMM"), todoIetm.LastModified.ToString("hh:mm"));

					IsNew = false;

					Name = "Edit Note";

                });
				

			}));

			
            return 0;
        }

		/// <summary>
        /// Updates the note.
        /// </summary>
		private async void UpdateNote()
        {
            await Task.Run((delegate
            {
				currentTodoItem.Content = Content;

				currentTodoItem.Status = (Status ? TodoStatus.Completed : TodoStatus.Pending);

				currentTodoItem.Picture = Helper.ReadBytes(ImageRaw);

				TodoItemDatabase.UpdateTodo(currentTodoItem);

            }));

            return;
        }

        /// <summary>
        /// Deletes the todo.
        /// </summary>
        private void DeleteTodo()
        {
            TodoItemDatabase.DeleteTodo(currentTodoItem);

            NavigateBack?.Invoke();

            return;
        }

        /// <summary>
        /// Shares the todo.
        /// </summary>
        private async void ShareTodo()
        {

            var title = "Todo Sample in Xamarin Forms";
            var url = "https://www.propelics.com/";

            try
            {
                await Plugin.Share.CrossShare.Current.Share(new Plugin.Share.Abstractions.ShareMessage
                {
                    Url = url,
                    Title = title,
                    Text = Content
                }, new Plugin.Share.Abstractions.ShareOptions { ChooserTitle = title });
            }
            catch (Exception ex)
            {
                Helper.Log(ex);
            }
        }

        /// <summary>
        /// Sees the image.
        /// </summary>
        private void SeeImage ()
        {
			NavigateToFullImage?.Invoke(currentTodoItem);
        }

    }
}
