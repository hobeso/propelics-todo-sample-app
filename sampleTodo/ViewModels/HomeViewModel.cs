﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using sampleTodo.Models;
using sampleTodo.Services.Database;
using Xamarin.Forms;

namespace sampleTodo.ViewModels
{
    public class HomeViewModel: INotifyPropertyChanged
    {
		public event PropertyChangedEventHandler PropertyChanged;
		
        public Action NewTodo;
		public Action<TodoItem> EditTodo;

		public Command AddTodoCommand { get; private set; }
        public Command TapPhotoCommand { get; private set; }
		public ICommand SearchCommand { get; protected set; }

        public ObservableCollection<GroupedTodoItemModel> grouped { get; set; }

        private TodoItem _SelectedItem { get; set; }
		public TodoItem SelectedItem
		{
			get
			{
				return _SelectedItem;
			}
			set
			{
				_SelectedItem = value;

				if (_SelectedItem == null)
					return;

				EditTodo(_SelectedItem);
			}
		}

		public string _searchText;
		public string SearchText
		{
			get { return _searchText; }
			set
			{
                _searchText = value;

				OnPropertyChanged("SearchText");
			}
		}

		private string _Name = "Todo Sample";
		public string Name
		{
			get { return _Name; }
			set
			{
				_Name = value;

				OnPropertyChanged("Name");
			}
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="T:sampleTodo.ViewModels.HomeViewModel"/> class.
        /// </summary>
		public HomeViewModel()
		{
            AddTodoCommand = new Command(CallNewTodo);
            TapPhotoCommand = new Command(CallNewTodo);
            SearchCommand = new Command<object>(SearchTodos);

            SearchText = "Please type to Search";

            LoadTodo();
		}

        /// <summary>
        /// Ons the property changed.
        /// </summary>
        /// <param name="name">Name.</param>
		void OnPropertyChanged(string name)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
		}

        /// <summary>
        /// Calls the new todo.
        /// </summary>
        void CallNewTodo()
        {
            NewTodo?.Invoke();
        }


        void SearchTodos(object search)
		{
			if (search is SearchBar)
				SearchLTodosByKey((search as SearchBar).Text);
		}

        public void SearchLTodosByKey(string search)
        {
            Debug.WriteLine("Searchig: " + search);

			if (string.IsNullOrEmpty(search))
			{
                Debug.WriteLine("LoadTodo");

                LoadTodo();
			}
			else
            {
                grouped.Clear();

				var pendingGroup = new GroupedTodoItemModel() { LongName = "My pending TODOs", ShortName = "P" };

				var completeGroup = new GroupedTodoItemModel() { LongName = "My completed TODOs", ShortName = "C" };

                var TodoItemsPendings = new ObservableCollection<TodoItem>(TodoItemDatabase.GetTodoPending().Where(x => x.Content.ToLower().Contains(search.ToLower())).ToList());

				var TodoItemsComplete = new ObservableCollection<TodoItem>(TodoItemDatabase.GetTodoComplete().Where(x => x.Content.ToLower().Contains(search.ToLower())).ToList());

				foreach (TodoItem item in TodoItemsPendings)
				{
					pendingGroup.Add(item);
				}

				foreach (TodoItem item in TodoItemsComplete)
				{
					completeGroup.Add(item);
				}

				grouped.Add(pendingGroup);

				grouped.Add(completeGroup);
            }
				

		}

			/// <summary>
			/// Loads the todo.
			/// </summary>
			public void LoadTodo()
        {
            if (grouped == null)
			    grouped = new ObservableCollection<GroupedTodoItemModel>();

			grouped.Clear();

            var pendingGroup = new GroupedTodoItemModel() { LongName = "My pending TODOs", ShortName = "P" };

            var completeGroup = new GroupedTodoItemModel() { LongName = "My completed TODOs", ShortName = "C" };

            var TodoItemsPendings = new ObservableCollection<TodoItem>(TodoItemDatabase.GetTodoPending());

            var TodoItemsComplete = new ObservableCollection<TodoItem>(TodoItemDatabase.GetTodoComplete());

			foreach (TodoItem item in TodoItemsPendings)
			{
                pendingGroup.Add(item);
			}

			foreach (TodoItem item in TodoItemsComplete)
			{
				completeGroup.Add(item);
			}

			grouped.Add(pendingGroup);

			grouped.Add(completeGroup);
        }
    }
}
