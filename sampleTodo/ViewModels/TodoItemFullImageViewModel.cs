﻿using System;
using System.ComponentModel;
using sampleTodo.Helpers;
using sampleTodo.Models;
using Xamarin.Forms;

namespace sampleTodo.ViewModels
{
	public class TodoItemFullImageViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private ImageSource _SourceImg;
		public ImageSource SourceImg
		{
			get { return _SourceImg; }
			set
			{
				_SourceImg = value;

				OnPropertyChanged("SourceImg");
			}
		}

		/// <summary>
		/// Ons the property changed.
		/// </summary>
		/// <param name="name">Name.</param>
		void OnPropertyChanged(string name)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
		}

		/// <summary>
        /// Initializes a new instance of the <see cref="T:sampleTodo.ViewModels.TodoItemFullImageViewModel"/> class.
        /// </summary>
        /// <param name="currentTodoItem">Current todo item.</param>
        public TodoItemFullImageViewModel(TodoItem currentTodoItem)
        {
            SourceImg = Helper.ReadBase64(currentTodoItem.Picture);
        }
    }
}
