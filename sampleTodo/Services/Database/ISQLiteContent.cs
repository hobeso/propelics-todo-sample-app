﻿using SQLite.Net;
using SQLite.Net.Interop;

namespace sampleTodo.Services.Database
{
    /// <summary>
    /// SQLite content.
    /// </summary>
	public interface ISQLiteContent
	{
		SQLiteConnection GetConnection();

		string GetPath();

		ISQLitePlatform GetPlatform();
	}
}
