﻿using System;
using System.Collections.Generic;
using sampleTodo.Helpers;
using sampleTodo.Models;
using SQLite.Net;
using Xamarin.Forms;

namespace sampleTodo.Services.Database
{
    public class TodoItemDatabase
    {
        /// <summary>
        /// Gets or sets the connection database.
        /// </summary>
        /// <value>The connection database.</value>
        public SQLiteConnection ConnectionDatabase { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:sampleTodo.Services.Database.TodoItemDatabase"/> class.
        /// </summary>
        public TodoItemDatabase()
        {
			ConnectionDatabase = DependencyService.Get<ISQLiteContent>().GetConnection();
            ConnectionDatabase.CreateTable<TodoItem>();
        }

        /// <summary>
        /// Todos the item database manager.
        /// </summary>
        /// <returns>The item database manager.</returns>
		private static TodoItemDatabase TodoItemDatabaseManager()
		{
			return new TodoItemDatabase();
		}

        /// <summary>
        /// Adds the todo.
        /// </summary>
        /// <returns><c>true</c>, if todo was added, <c>false</c> otherwise.</returns>
        /// <param name="todoItem">Todo item.</param>
        public static bool AddTodo(TodoItem todoItem)
        {
            var database = TodoItemDatabaseManager();

            todoItem.LastModified = DateTime.UtcNow;

            database.ConnectionDatabase.Insert(todoItem);

            return true;
        }

        /// <summary>
        /// Updates the todo.
        /// </summary>
        /// <returns><c>true</c>, if todo was updated, <c>false</c> otherwise.</returns>
        /// <param name="todoItem">Todo item.</param>
		public static bool UpdateTodo(TodoItem todoItem)
		{
            try
            {
				var database = TodoItemDatabaseManager();

				todoItem.LastModified = DateTime.UtcNow;

				database.ConnectionDatabase.Update(todoItem);


			}
			catch (Exception ex)
			{
				Helper.Log(ex);
			}

			return true;
		}

        /// <summary>
        /// Deletes the todo.
        /// </summary>
        /// <returns><c>true</c>, if todo was deleted, <c>false</c> otherwise.</returns>
        /// <param name="todoItem">Todo item.</param>
		public static bool DeleteTodo(TodoItem todoItem)
		{
            try
            {
				var database = TodoItemDatabaseManager();

				database.ConnectionDatabase.Delete(todoItem);
			}
            catch (Exception ex)
            {
                Helper.Log(ex);
            }

			return true;
		}

        /// <summary>
        /// Gets the todo.
        /// </summary>
        /// <returns>The todo.</returns>
		public static List<TodoItem> GetTodo()
		{
            try
            {
				var database = TodoItemDatabaseManager();

				var lst = database.ConnectionDatabase.Query<TodoItem>("SELECT * FROM [TodoItem] order by Status,LastModified desc ");

				return lst; 
            }
			catch (Exception ex)
			{
				Helper.Log(ex);

                return null;
			}
			
		}

        /// <summary>
        /// Gets the todo pending.
        /// </summary>
        /// <returns>The todo pending.</returns>
		public static List<TodoItem> GetTodoPending()
		{
            try
            {

				var database = TodoItemDatabaseManager();

				var lst = database.ConnectionDatabase.Query<TodoItem>("SELECT * FROM [TodoItem] where Status = '0' order by LastModified desc ");

				return lst;
			}
			catch (Exception ex)
			{
				Helper.Log(ex);

				return null;
			}
		}

        /// <summary>
        /// Gets the todo complete.
        /// </summary>
        /// <returns>The todo complete.</returns>
		public static List<TodoItem> GetTodoComplete()
		{
            try
            {

				var database = TodoItemDatabaseManager();

				var lst = database.ConnectionDatabase.Query<TodoItem>("SELECT * FROM [TodoItem] where Status = '1'  order by LastModified desc ");

				return lst;
            }
			catch (Exception ex)
			{
				Helper.Log(ex);

				return null;
			}
		}
    }
}
