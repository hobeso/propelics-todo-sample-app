﻿using System;
using System.Collections.Generic;
using sampleTodo.Models;
using sampleTodo.ViewModels;
using Xamarin.Forms;

namespace sampleTodo.Views.TodoFullImage
{
    public partial class TodoItemFullImagePage : ContentPage
    {
        public TodoItemFullImagePage(TodoItem currentTodoItem)
        {
            InitializeComponent();

            BindingContext = new TodoItemFullImageViewModel(currentTodoItem);
        }
    }
}
