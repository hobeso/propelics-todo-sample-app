﻿
using System;
using sampleTodo.Models;
using sampleTodo.ViewModels;
using Xamarin.Forms;

namespace sampleTodo.Views.TodoPage
{
    public partial class TodoItemPage : ContentPage
    {
        private TodoItemViewmodel Viewmodel { get; set; }

        public TodoItemPage(TodoItem SelectedItem = null)
        {
            InitializeComponent();

            BindingContext = Viewmodel = new TodoItemViewmodel(SelectedItem);

            Viewmodel.NavigateBack += NavigateBack;

            Viewmodel.NavigateToFullImage += NavigateToFullImage;
         }

        /// <summary>
        /// Navigates the back when the To do item is deleted
        /// </summary>
        private void NavigateBack()
        {
            Navigation.PopAsync(true);
        }

        /// <summary>
        /// Navigates to full image.
        /// </summary>
        /// <param name="SelectedItem">Selected item.</param>
        private async void NavigateToFullImage(TodoItem SelectedItem)
        {
            await Navigation.PushAsync(new TodoFullImage.TodoItemFullImagePage(SelectedItem));
        }
    }
}
