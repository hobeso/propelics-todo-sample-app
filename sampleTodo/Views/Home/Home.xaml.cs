﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using sampleTodo.Models;
using sampleTodo.Services.Database;
using sampleTodo.ViewModels;
using sampleTodo.Views.TodoPage;
using Xamarin.Forms;

namespace sampleTodo.Views.Home
{
    public partial class Home : ContentPage
    {
        public Home()
        {
            InitializeComponent();
        }

        private HomeViewModel Viewmodel { get; set; }

		protected override void OnAppearing()
		{
			base.OnAppearing();

            BindingContext = Viewmodel = new HomeViewModel();

            Viewmodel.NewTodo += OnSaveClicked;
            Viewmodel.EditTodo += OnEditClicked;
		}

		protected override void OnDisappearing()
		{
			TodoListView.SelectedItem = null;
		}

		// We Pass the data to the Bising controller
		void OnValueChanged(object sender, TextChangedEventArgs e)
		{
			Viewmodel.SearchLTodosByKey(e.NewTextValue);
		}

        /// <summary>
        /// Ons the save clicked.
        /// </summary>
		private async void OnSaveClicked()
		{
            await Navigation.PushAsync(new TodoItemPage());
		}

        /// <summary>
        /// Ons the edit clicked.
        /// </summary>
        /// <param name="SelectedItem">Selected item.</param>
        private async void OnEditClicked(TodoItem SelectedItem)
		{
			await Navigation.PushAsync(new TodoItemPage(SelectedItem));
		}

    }
}
