﻿using sampleTodo.Views.Home;
using Xamarin.Forms;

namespace sampleTodo
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new sampleTodoPage();

			var nav = new NavigationPage(new Home());

			nav.BarBackgroundColor = Color.FromHex("007196");
			nav.BarTextColor = Color.White;

			MainPage = nav;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
