﻿using System.Collections.ObjectModel;

namespace sampleTodo.Models
{
	public class GroupedTodoItemModel : ObservableCollection<TodoItem>
	{
		public string LongName { get; set; }
		public string ShortName { get; set; }
	}
}
