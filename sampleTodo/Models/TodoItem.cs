﻿using System;
using sampleTodo.Helpers;
using SQLite.Net.Attributes;
using Xamarin.Forms;

namespace sampleTodo.Models
{
    public class TodoItem
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Content { get; set; }
        public TodoStatus Status { get; set; }
        public string Picture { get; set; }
		public ImageSource PictureSource {
            get
            {
                try
                {
					if (!string.IsNullOrEmpty(Picture))
						return Helper.ReadBase64(Picture);
					else
						return new FileImageSource { File = Constants.ImageDefault }; 
                }
                catch
                {
                    return new FileImageSource { File = Constants.ImageDefault };
				}
               
            }
        
        }
        public DateTime LastModified { get; set; }

        public TodoItem ()
        {
            Picture = Constants.ImageDefault;


        }
	}

    public enum TodoStatus
    {
        Pending = 0,
        Completed = 1
    }

	
}
